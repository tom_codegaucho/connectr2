/*
 * Tom Murphy
 *
 * 10/21/2014 - created
 *
 * This module will provide a mechanism for displaying user info for those systems using bootstrap.
 *
 * common.js contains the requirejs.config needed by this module
 *
 *  Specific Pre-requisites:
 *       Bootstrap:  If using the bootstrap modal, we are going to need to load bootstrap's js and css file
 *
 */

define(['./common.js'], function () {
    //do initialization stuff here
    //we can pre-compile
    /*
    require(["hogan", "text!/Content/userinfo.html"], function (Hogan, template) {
                _obj.userTemplate_compiled = Hogan.compile(template);
    });
    */

    //load the module specific css
    require(['css!../css/connectr2.css'], function () {
        //don't block while we wait for this - it will be there by the tme the user clicks the button
    });

    var bootstrap3_enabled = (typeof $().emulateTransitionEnd == 'function');
    alert("bootstrap3_enabled = " + bootstrap3_enabled);
    

    return {
	//where are we getting the data for a user
	adserviceurl: "https://x0202tnythnetpd.aa.ad.epa.gov/adservice/api/users/ADUser/",

        //we are keeping a hash of all users, so we don't request info for the same user more than once
        userHash: {},

        //the inner template.  
        userTemplate_compiled: null,

        atLeft: function (sourceStr, keyStr) {
            return (sourceStr.indexOf(keyStr) == -1 | keyStr == '') ? '' : sourceStr.split(keyStr)[0];
        },

        atRight: function (sourceStr, keyStr) {
            idx = sourceStr.indexOf(keyStr);
            return (idx == -1 | keyStr == '') ? '' : sourceStr.substr(idx + keyStr.length);
        },

        addUserToHash: function (identifier, modaltype, theme) {
            //given a user, and a user type, will fetch their info from active directory, add them to our hash, and then trigger our modal (depending on type)
            var _obj = this;
            //can be called with one parameter or two.  If one is given, assumes it is a ename
            //given a users lanid or upn, will get their info from Active Directory
            //identifier will be the identifier - either the lanid or the upn

            //modaltype will tell us what we are doing with this once fetched

            var jsonpurl = _obj.adserviceurl + identifier + "?callback=?";

            $.getJSON(jsonpurl, function (json) {
                //first thing set hash - we don't care what type it is
                _obj.userHash[identifier] = json;

                //now display the info
                //userInfoModal.replaceDataAndDisplayModal(json);
		_obj.displayUserModal_Bootstrap(identifier);

            });
        },

        // **********************************************************
        // ***   Bootstrap specific bits
        // ***
        // *** NOTE: template for the Bootstrap Modal is kept in a html file at  Content/bootstrapModal.html
        // **********************************************************

	bootstrapModal_created: false,

        displayUserModal_Bootstrap: function (identifier) {
            //identifier - users lanid or upn
            //will pop up a Bootstrap modal with this users info inside
            var _obj = this;

            //first see if we already have info on this user
            if (_obj.userHash.hasOwnProperty(identifier)) {
                //we have info on this user, see if a modal already exists
                if (_obj.bootstrapModal_created) {
                    _obj.showBootstrapModal(_obj.userHash[identifier]);
                } else {
                    _obj.createBootstrapModal(_obj.userHash[identifier]);
                }
            } else {
                //no info on this user - go get some
                _obj.addUserToHash(identifier, "bootstrapModal")
            }
        },

        
        createBootstrapModal: function (userjson) {
            var _obj = this;
	    
            require(["text!../templates/bootstrapModal.html"], function (template) {
                //we don't need a real template for the modal, so just inject it.
                //but perhaps we should compile it to give the bootstrapModal a unique identifier or allow us to add other things?
                $("body").append(template);
                _obj.bootstrapModal_created = true;

                //here we need to hook up the typeahead box to allow them to lookup the info on a different user

                //now render the template into the modal div
                _obj.showBootstrapModal(userjson);
            });
        },

        showBootstrapModal: function (userjson) {
            var _obj = this;
            require(["jquery", "hogan", "text!../templates/userinfo.html", "bootstrap"], function ($, Hogan, template) {
                if (_obj.userTemplate_compiled == null) {
                    //must be first time - compile template
                    _obj.userTemplate_compiled = Hogan.compile(template);
                }

                var userHTML = _obj.userTemplate_compiled.render(userjson);

                //now stick in the modal
                //these should NOT use specific ids, instead they should use classes inside the modal #userModal.modal-body, #userModal.modal-title
                $("#userModalTitle").html("User Info: " + userjson.cn);
                $("#userModalBody").html(userHTML);

                //and change the class on the modal so it is visible
                //should be the unique id we create when we render the template
                $('#userModal').modal('show');

                //here we need to make the supervisor name clickable
                _obj.processSelector(".userInfoDialog_childModal", "bootstrapModal");

            });
        },

        // **********************************************************
        // ***   The portion that processes the document and turns the tags into links or adds buttons
        // **********************************************************
        processSelector: function (selector, modalType) {
            var _obj = this;
            require(["jquery"], function () {
                //selector - a selector to find all the elements that we may want to make active for connectR2 dialogs
                //modalType - normally we don't provide this, but if we do, we will not bother checking for the data-userinfo-identifier field - usually used for child modals
                selector = (selector == undefined) ? "[data-userinfo-identifier]" : selector + "[data-userinfo-identifier]";

                $(selector).each(function (index, element) {
                    _obj.processElement(element, modalType);

                });
            });
            
        },

        processElement: function (element, modalType) {
            var _obj = this;
            require(["jquery"], function ($) {
                //get identifier but remove all leading and trailing spaces
                //.trim() no work in IE9
                //var identifier = $(element).attr("data-userinfo-identifier").trim();
                var identifier = $(element).attr("data-userinfo-identifier");

                //get link display type
                var linkDisplay = $(element).attr("data-userinfo-linkdisplay");
                linkDisplay = (linkDisplay == undefined) ? "image" : linkDisplay;

                //get modalType - used on selector of for each, so must be there
                //in some cases (child modals, for instance), we don't care if the element specifies a modalType - we are going to use a specified type
                modalType = (modalType == undefined) ? $(element).attr("data-userinfo-dialog") : modalType;

                if (linkDisplay == "clickable") {
                    $(element).click(function () {
                        _obj.userInfoClickHandler(modalType, identifier);
                    });
                } else {
                    //must be an image link
                    var imgurl = $(element).attr("data-userinfo-img");
                    //give it a default image if none defined
		    imgurl = (imgurl == undefined) ? "../images/info.png" : imgurl;
		    

		    /*		    
                    //can't just create and stick in an image tag, as url will be incorrect, so use the image plugin
                    //but couldn't get this working right now, so hold off
		    require(["image!../images/info.png"], function(clkImg) {
			clkImg.click(function () {
                       		_obj.userInfoClickHandler(modalType, identifier);
                    	});

                    	clkImg.appendTo(element);
		    });
		    */
                    

		    //the old way of doing it - results in incorrect image path
		    var clkImg = $('<img class="userInfoIcon">');
                    clkImg.attr("src", imgurl);
                    clkImg.click(function () {
                        _obj.userInfoClickHandler(modalType, identifier);
                    });

                    clkImg.appendTo(element);

                }

            });
            
        },

        userInfoClickHandler: function (modalType, identifier, theme) {
            var _obj = this;
	    _obj.displayUserModal_Bootstrap(identifier);
            
        }
    }
});