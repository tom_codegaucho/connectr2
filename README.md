connectr2
==========

connectr2 is a requirejs module that should let us show user information virtually anywhere.  We have tested this successfully in ASP.NET applications, SharePoint, Java, plain-Jane HTML, ..

Version
-------
0.0


Author
--------
Tom Murphy (murphy.tom@epa.gov) wrote the initial version, Miriam Finestone (finestone.miriam@epa.gov will be taking it over from here.

Tech
-----
connectr2 requires that you load common.js containing a requirejs.config file pointing to all the libraries and plugins that are needed.  Note that we specify the paths as an array, with a cdn listed first, and the path to the local file as a fallback.

* [requirejs] - what lets this all happen
* [jquery] - ajax and general dom manipulations - use 1.11 as earlier versions are NOT AMD
* [jquery-ui] - used to create the jquery-ui dialog often used
* [domReady] - allows us to stall execution until the dom is loaded
* [bootstrap] - needed if we are going to be creating a bootstrap modal
* [hogan] - for compiling and rendering our templates
* [text] - require-text - a plugin for loading text resources (primarily our templates)
* [image] - plugin to allow us to resolve our local image path (for the i icon next to a name)
* [css] - allows us to load a module specific css file

If you are using the Bootstrap Modal, we are assuming that you are already loading Bootstrap's js and css already

If you are using the dojo dialog, we are assuming that you have dojo and the css needed for the dojo dialog.

Attributes
----------
#####data-userinfo-dialog
allowable values: 
* jqueryUIDialog - the jquery ui dialog.   Will use page's theme if non-specified
* bootstrapModal - gives us that nice bootstrap modal, with whatever styling we have used on the page
* dojoDialog - will give us a dojo dialog (module needs to be loaded via dojo, and the appropriate css needs to be loaded)

determines the type of dialog/modal that is displayed

#####data-userinfo-theme
allowable values: black-tie blitzer cupertino dark-hive dot-luv eggplant excite-bike flick hot-sneaks humanity le-frog mint-choc overcast pepper-grinder redmond smoothness south-street start sunny swanky-purse trontastic ui-darkness ui-lightness vader
if not specified, will inherit whatever the page is using

#####data-userinfo-linkdisplay
allowable values: 
* clickable - turns the element into a hotspot. 
* image - adds a clickable image as last element in node.  If no image url specified, a default will be used

#####data-userinfo-identifier
the lan id or upn (email address) of the user.  This will be looked up in Active Directory

#####data-userinfo-img
the url of the image that should be used for a element where the data-userinfo-linkdisplay = image
if none specified, will use default

Installation Example

--------------


```sh


<p data-userinfo-dialog="jqueryUIDialog" data-userinfo-linkdisplay="image" data-userinfo-identifier="mindlin.vadim@epa.gov" data-userinfo-theme="redmond">jquery UI image for Vadim Mindlin</p>

<p>jquery UI clickable for <a data-userinfo-dialog="jqueryUIDialog" data-userinfo-linkdisplay="clickable" data-userinfo-identifier="mindlin.vadim@epa.gov" data-userinfo-theme="redmond">Vadim Mindlin</a></p>

<script src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.1.15/require.min.js"></script>
    
<script>
		require(['//x0202tnythnetpd.aa.ad.epa.gov/common/connectr2/connectr2.js'], function (ConnectR2) {
    			ConnectR2.processSelector();
		});
</script>
```



License

----


MIT

[requirejs]:http://requirejs.org/
[jquery]:http://jquery.com/
[jquery-ui]:http://jqueryui.com/
[domReady]:https://github.com/requirejs/domready
[bootstrap]:http://getbootstrap.com/
[hogan]:http://twitter.github.io/hogan.js/
[text]:https://github.com/requirejs/text
[image]:https://github.com/millermedeiros/requirejs-plugins
[css]:https://github.com/guybedford/require-css