/*
 * Tom Murphy
 *
 * 10/21/2014 - created
 *
 * This module will provide a mechanism for displaying user info
 * options will be given to allow bootstrap, jquery, dojo, and other dialog types
 * each of these types should be split into their own module
 *
 * common.js contains the requirejs.config needed by this module
 *
 *  Specific Pre-requisites:
 *       Bootstrap:  If using the bootstrap modal, we are going to need to load bootstrap's js and css file
 *       Dojo: If using the dojo dialog, we are going to need to load dojo and the css needed for the dialog.  Also a theme must be specified in some wrapper (usually body)
 *
 */

define(['./common.js'], function (common) {
    //do initialization stuff here
    //we can pre-compile
    /*
    require(["hogan", "text!/Content/userinfo.html"], function (Hogan, template) {
                _obj.userTemplate_compiled = Hogan.compile(template);
    });
    */

    //load the module specific css
    require(['css!../css/connectr2.css'], function () {
        //don't block while we wait for this - it will be there by the tme the user clicks the button
    });
    

    return {
	//where are we getting the data for a user
	adserviceurl: "https://x0202tnythnetpd.aa.ad.epa.gov/adservice/api/users/ADUser/",

        //we are keeping a hash of all users, so we don't request info for the same user more than once
        userHash: {},

        //the inner template.  
        userTemplate_compiled: null,

        atLeft: function (sourceStr, keyStr) {
            return (sourceStr.indexOf(keyStr) == -1 | keyStr == '') ? '' : sourceStr.split(keyStr)[0];
        },

        atRight: function (sourceStr, keyStr) {
            idx = sourceStr.indexOf(keyStr);
            return (idx == -1 | keyStr == '') ? '' : sourceStr.substr(idx + keyStr.length);
        },

        addUserToHash: function (identifier, modaltype, theme) {
            //given a user, and a user type, will fetch their info from active directory, add them to our hash, and then trigger our modal (depending on type)
            var _obj = this;
            //can be called with one parameter or two.  If one is given, assumes it is a ename
            //given a users lanid or upn, will get their info from Active Directory
            //identifier will be the identifier - either the lanid or the upn

            //modaltype will tell us what we are doing with this once fetched

            var jsonpurl = _obj.adserviceurl + identifier + "?callback=?";

            $.getJSON(jsonpurl, function (json) {
                //first thing set hash - we don't care what type it is
                _obj.userHash[identifier] = json;

                //now display the info
                //userInfoModal.replaceDataAndDisplayModal(json);
                switch (modaltype) {
                    case "bootstrapModal":
                        _obj.displayUserModal_Bootstrap(identifier);
                        break;
                    case "dojoDialog":
                        _obj.displayUser_DojoDialog(identifier);
                        break;
                    case "jqueryUIDialog":
                        _obj.displayUser_JQueryUIDialog(identifier, theme);
                        break;
                    case "raw":
                        alert("raw");
                        break;
                }
            });
        },

        // **********************************************************
        // ***   Bootstrap specific bits
        // ***
        // *** NOTE: template for the Bootstrap Modal is kept in a html file at  Content/bootstrapModal.html
        // **********************************************************

	bootstrapModal_created: false,

        displayUserModal_Bootstrap: function (identifier) {
            //identifier - users lanid or upn
            //will pop up a Bootstrap modal with this users info inside
            var _obj = this;

            //first see if we already have info on this user
            if (_obj.userHash.hasOwnProperty(identifier)) {
                //we have info on this user, see if a modal already exists
                if (_obj.bootstrapModal_created) {
                    _obj.showBootstrapModal(_obj.userHash[identifier]);
                } else {
                    _obj.createBootstrapModal(_obj.userHash[identifier]);
                }
            } else {
                //no info on this user - go get some
                _obj.addUserToHash(identifier, "bootstrapModal")
            }
        },

        
        createBootstrapModal: function (userjson) {
            var _obj = this;
	    
            require(["text!../templates/bootstrapModal.html"], function (template) {
                //we don't need a real template for the modal, so just inject it.
                //but perhaps we should compile it to give the bootstrapModal a unique identifier or allow us to add other things?
                $("body").append(template);
                _obj.bootstrapModal_created = true;

                //here we need to hook up the typeahead box to allow them to lookup the info on a different user

                //now render the template into the modal div
                _obj.showBootstrapModal(userjson);
            });
        },

        showBootstrapModal: function (userjson) {
            var _obj = this;
            require(["jquery", "hogan", "text!../templates/userinfo.html", "bootstrap"], function ($, Hogan, template) {
                if (_obj.userTemplate_compiled == null) {
                    //must be first time - compile template
                    _obj.userTemplate_compiled = Hogan.compile(template);
                }

                var userHTML = _obj.userTemplate_compiled.render(userjson);

                //now stick in the modal
                //these should NOT use specific ids, instead they should use classes inside the modal #userModal.modal-body, #userModal.modal-title
                $("#userModalTitle").html("User Info: " + userjson.cn);
                $("#userModalBody").html(userHTML);

                //and change the class on the modal so it is visible
                //should be the unique id we create when we render the template
                $('#userModal').modal('show');

                //here we need to make the supervisor name clickable
                _obj.processSelector(".userInfoDialog_childModal", "bootstrapModal");

            });
        },

        // **********************************************************
        // ***   Dojo Dialog specific bits
        // **********************************************************

        dojoDialog: null,

        displayUser_DojoDialog: function (identifier) {
            //identifier - users lanid or upn
            //will pop up a Dojo Dialog with this users info inside
            var _obj = this;

            //first see if we already have info on this user
            if (_obj.userHash.hasOwnProperty(identifier)) {
                //we have info on this user, see if a modal already exists
                if (_obj.dojoDialog == null) {
                    _obj.createDojoDialog(_obj.userHash[identifier]);
                } else {
                    _obj.showDojoDialog(_obj.userHash[identifier]);
                }
            } else {
                //no info on this user - go get some
                _obj.addUserToHash(identifier, "dojoDialog")
            }
        },

        createDojoDialog: function (userjson) {
            var _obj = this;
            require(["dijit/Dialog"], function (Dialog) {
                _obj.dojoDialog = new Dialog({
                    title: "User Information: " + userjson.displayName,
                    //content: _obj.userTemplate_compiled.render(userjson),
                    content: "getting user info",
                    style: "width: 600px"
                });

                _obj.showDojoDialog(userjson);
            });
        },

        showDojoDialog: function (userjson) {
            var _obj = this;

	    //as this is loading dojo, we can use the dojo/text here
            require(["hogan", "text!../templates/userinfo.html"], function (Hogan, template) {
                if (_obj.userTemplate_compiled == null) {
                    //must be first time - compile template
                    _obj.userTemplate_compiled = Hogan.compile(template);
                }

                var userHTML = _obj.userTemplate_compiled.render(userjson);

                _obj.dojoDialog.set("title", "User Information: " + userjson.displayName)
                _obj.dojoDialog.set("content", userHTML)
                _obj.dojoDialog.show();
            });
        },


        // **********************************************************
        // ***   JQueryUI Dialog specific bits
        // **********************************************************

        jqueryUIDialog_created: false,

        displayUser_JQueryUIDialog: function (identifier, theme) {
            //identifier - users lanid or upn
            //will pop up a JQueryUIDialog with this users info inside
            var _obj = this;

            //first see if we already have info on this user
            if (_obj.userHash.hasOwnProperty(identifier)) {
                //we have info on this user, see if a modal already exists
                if (_obj.jqueryUIDialog_created) {
                    _obj.showJQueryUIDialog(_obj.userHash[identifier]);
                } else {
                    _obj.createJQueryUIDialog(_obj.userHash[identifier], theme);
                }
            } else {
                //no info on this user - go get some
                _obj.addUserToHash(identifier, "jqueryUIDialog", theme);
            }
        },

        createJQueryUIDialog: function (userjson, theme) {
            //NOTE:  use of jquery UI requires a theme stylesheet - 
            // the page builder may have already loaded it, in which case we don't need to pass a theme
            // or they could have passed in a theme, in which case we should load it now.we could load it here
            var _obj = this;
            //ideally we would have jquery-ui stored with the dialog in its own file - and we would ask for this using "jquery-ui/dialog
            //as I currently have it pointing at the cdn which is one file, we specify it this way
            var requireArray = ["jquery", "jquery-ui"];
            if (theme != undefined) {
                //a theme is being passed in, go ahead and load it up
                //themes include
                // black-tie blitzer cupertino dark-hive dot-luv eggplant excite-bike flick hot-sneaks 
                //humanity le-frog mint-choc overcast pepper-grinder redmond smoothness south-street start sunny swanky-purse trontastic ui-darkness ui-lightness vader
                var cssurl = "css!//code.jquery.com/ui/1.11.2/themes/" + theme + "/jquery-ui.css";
                requireArray.push(cssurl);
            }

            require(requireArray, function ($) {
                $("body").append('<div id="userInfoJQueryUIDialog" title="User Information"><p id="userInfoJQueryUIDialog_body">some such stuff</p></div>');
                $("#userInfoJQueryUIDialog").dialog({
                    autoOpen: false,
                    modal: true,
                    minWidth: 600
                });
                _obj.jqueryUIDialog_created = true;

                //now render the template into the modal div
                _obj.showJQueryUIDialog(userjson);
            });

        },

        showJQueryUIDialog: function (userjson) {
            var _obj = this;
	    require(["hogan", "text!../templates/userinfo.html", "jquery", "jquery-ui"], function (Hogan, template, $) {
                if (_obj.userTemplate_compiled == null) {
                    //must be first time - compile template
                    _obj.userTemplate_compiled = Hogan.compile(template);
                }

                var userHTML = _obj.userTemplate_compiled.render(userjson);

                $("#userInfoJQueryUIDialog_body").html(userHTML);

                $("#userInfoJQueryUIDialog").dialog("open");


            });
        },


        // **********************************************************
        // ***   The portion that processes the document and turns the tags into links or adds buttons
        // **********************************************************
        processSelector: function (selector, modalType) {
            var _obj = this;
            require(["jquery"], function () {
                //selector - a selector to find all the elements that we may want to make active for connectR2 dialogs
                //modalType - normally we don't provide this, but if we do, we will not bother checking for the data-userinfo-identifier field - usually used for child modals
                selector = (selector == undefined) ? "[data-userinfo-identifier]" : selector + "[data-userinfo-identifier]";

                $(selector).each(function (index, element) {
                    _obj.processElement(element, modalType);

                });
            });
            
        },

        processElement: function (element, modalType) {
            var _obj = this;
            require(["jquery"], function ($) {
                //get identifier but remove all leading and trailing spaces
                //.trim() no work in IE9
                //var identifier = $(element).attr("data-userinfo-identifier").trim();
                var identifier = $(element).attr("data-userinfo-identifier");

                //get link display type
                var linkDisplay = $(element).attr("data-userinfo-linkdisplay");
                linkDisplay = (linkDisplay == undefined) ? "image" : linkDisplay;

                //get modalType - used on selector of for each, so must be there
                //in some cases (child modals, for instance), we don't care if the element specifies a modalType - we are going to use a specified type
                modalType = (modalType == undefined) ? $(element).attr("data-userinfo-dialog") : modalType;

                //get theme - used on jqueryui stuff
                //leave undefined if not set - we are going to pick up from pages css
                // black-tie blitzer cupertino dark-hive dot-luv eggplant excite-bike flick hot-sneaks humanity le-frog mint-choc overcast 
                // pepper-grinder redmond smoothness south-street start sunny swanky-purse trontastic ui-darkness ui-lightness vader
                var theme = $(element).attr("data-userinfo-theme");
                //theme = (theme == undefined) ? "redmond" : theme;

                if (linkDisplay == "clickable") {
                    $(element).click(function () {
                        _obj.userInfoClickHandler(modalType, identifier, theme);
                    });
                } else {
                    //must be an image link
                    var imgurl = $(element).attr("data-userinfo-img");
                    //give it a default image if none defined
		    imgurl = (imgurl == undefined) ? "../images/info.png" : imgurl;
		    

		    /*		    
                    //can't just create and stick in an image tag, as url will be incorrect, so use the image plugin
                    //but couldn't get this working right now, so hold off
		    require(["image!../images/info.png"], function(clkImg) {
			clkImg.click(function () {
                       		_obj.userInfoClickHandler(modalType, identifier, theme);
                    	});

                    	clkImg.appendTo(element);
		    });
		    */
                    

		    //the old way of doing it - results in incorrect image path
		    var clkImg = $('<img class="userInfoIcon">');
                    clkImg.attr("src", imgurl);
                    clkImg.click(function () {
                        _obj.userInfoClickHandler(modalType, identifier, theme);
                    });

                    clkImg.appendTo(element);

                }

            });
            
        },

        userInfoClickHandler: function (modalType, identifier, theme) {
            var _obj = this;
            switch (modalType) {
                case "bootstrapModal":
                    _obj.displayUserModal_Bootstrap(identifier);
                    break;
                case "dojoDialog":
                    _obj.displayUser_DojoDialog(identifier);
                    break;
                case "jqueryUIDialog":
                    _obj.displayUser_JQueryUIDialog(identifier, theme);
                    break;
                case "raw":
                    alert("raw");
                    break;
            }
        }
    }
});