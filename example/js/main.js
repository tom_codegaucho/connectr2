//in this example I only need jquery so I can load bootstrap
//and I only need bootstrap so that I can demo the bootstrap modal

//if a particular application is not using the bootstrap modal, we don't need to load bootstrap (bootstrap is not yet AMD)


require(['//x0202tnythnetpd.aa.ad.epa.gov/common/connectr2/connectr2.js', '//code.jquery.com/jquery-1.11.1.min.js', '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'], function (ConnectR2, $) {
    ConnectR2.processSelector();
})
