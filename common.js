requirejs.config({
    baseUrl: "//x0202tnythnetpd.aa.ad.epa.gov/common/connectr2/lib",
    paths: {
        app: '../',
	bootstrap: [
		"//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min",
		"bootstrap.min"
	],
        domReady: [ 
		"//cdnjs.cloudflare.com/ajax/libs/require-domReady/2.0.1/domReady.min",
		"domReady.min"
	],
	hogan: [
		"//cdnjs.cloudflare.com/ajax/libs/hogan.js/3.0.0/hogan.min.amd",
		"hogan.min.amd"
	],
        jquery: [
		"//code.jquery.com/jquery-1.11.1.min",
		"jquery-1.11.1.min"
	],
        "jquery-ui": [
		"//code.jquery.com/ui/1.11.2/jquery-ui.min",
		"jquery-ui.min"
	],
        text: [
		"//cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text.min",
		"text.min"
	], 
	image: [
		"//cdnjs.cloudflare.com/ajax/libs/requirejs-plugins/1.0.3/image.min",
		"image.min"
	]
    },
    shim: {
        "bootstrap": {
            "deps": ['jquery']
        }
    },
    map: {
        '*': {
            	'css': '//cdnjs.cloudflare.com/ajax/libs/require-css/0.1.5/css.min.js'
		//'css': 'css.min.js'
        }
    },
    config: {
        text: {
            useXhr: function (url, protocol, hostname, port) {
                // allow cross-domain requests
                // remote server allows CORS
                return true;
            }
        }
    }
});